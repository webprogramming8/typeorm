import { Column, Entity, PrimaryColumn } from "typeorm";

@Entity()
export class Product{
    @PrimaryColumn({name: "product_id"})
    id: number ;

    @Column({name: "product_name"})
    name: string;

    @Column({name: "product_price "})
    price: number;
}
